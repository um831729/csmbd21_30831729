import pandas as pd

''' Inital step is to define mapper and reducer functions
 utilising the yield function, as it generates an object that can be iterated over to get the data. '''
def map(record):
    passenger_id = record[0]
    yield (passenger_id, 1)

def reducer(key, values):
    total_flights = sum(values)
    yield (key, total_flights)

def run_sequential():
    '''Next we create a pandas dataframe that read the csv file '''
    dataframe = pd.read_csv('data/AComp_Passenger_data_no_error(1).csv', header=None)

    # Running sequential implementation with shuffler
    results = {}
    for record in dataframe.itertuples(index=False):
        for key, value in map(record):
            if key not in results:
                results[key] = []
            results[key].append(value)

    output = [(key, sum(values)) for key, values in results.items()]

    ''' We create a new dataframe to Save sequential output to csv file with passenger id and number of flight
        Next we sort the vales by decending '''
    New_Dataframe = pd.DataFrame(output, columns=['Passenger ID', 'Number of Flights'])
    New_Dataframe = New_Dataframe.sort_values(by='Number of Flights', ascending=False)
    New_Dataframe.to_csv('output_seq.csv', index=False)

    # Print passenger with the highest number of flights using the new dataframe.
    print(f"Passenger ID {New_Dataframe.iloc[0]['Passenger ID']} has travelled the most with {New_Dataframe.iloc[0]['Number of Flights']} flights.")

if __name__ == '__main__':
    run_sequential()
