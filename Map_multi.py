from functools import reduce
import multiprocessing as mp


class Job:
    def __init__(self, data_file_path):
        self.data_file_path = data_file_path

    def Map_in(self, x):#Map data files into key value pairs
        MapList = []
        # Creating loop to iterate through file and map
        for record in open(self.data_file_path):
            record = record.strip() # stripping for any white space
            record = record.split(',') # splitting data from the file by comma
            result = record[x] #Retreiving the required record
            MapList.append(result) # Appending result
        return MapList

    def map_func(self, x):
        """ Data aggregations and transformations 
        can be carried out using this function's output."""
        return x, 1

    def shuffle(self, mapper_output):
        data_dict = {}
        # This creates a key if doesnt exist
        for k, v in mapper_output:
            if k not in data_dict:
                data_dict[k] = [v]
            # Append the occurences value id key that already exist
            else:
                data_dict[k].append(v)
        return data_dict

    def reducer(self, x, y):
        '''returning the accumulated result reducer.'''
        return x + y

    def reduce_function(self, items):
        " The helps to get key values from items and return reduced value"
        key = items[0]
        values = items[1]
        reduced_values = reduce(self.reducer, values)
        return key, reduced_values

    def Run(self):
        with mp.Pool(processes=mp.cpu_count()) as pool:
            map_in = self.Map_in(0)# convert records to list
            chunk_size = int(len(map_in) / mp.cpu_count()) # calculate chunk size based on cpu
            map_out = pool.map(self.map_func, map_in, chunksize=chunk_size)
            reduce_in = self.shuffle(map_out)
            reduce_out = pool.map(self.reduce_function, reduce_in.items(), chunksize=chunk_size)
            reduce_out.sort(key=lambda x: x[1], reverse=True) # Sorting reduce_out values
            print(f"The passenger who travelled the most is- ID: {reduce_out[0][0]} with {reduce_out[0][1]} number of trips")
            with open('output.txt', 'w') as f:
                f.write(f'The passenger who travelled the most is- ID: {reduce_out[0][0]} with {reduce_out[0][1]} number of trips \n\n')
                f.write(f'Other passengers with highest number of flights in descending order\n\n')
                for record in reduce_out:
                    f.write(f'{record}\n')


if __name__ == '__main__':
    data_file_path = 'data/AComp_Passenger_data_no_error(1).csv'
    map_reducer = Job(data_file_path)
    map_reducer.Run()
